import { StyleSheet } from 'react-native'
import React, { useEffect, useState } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';
import { Provider, useDispatch } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { PersistStore, Store } from './redux/Store';
import NetInfo from "@react-native-community/netinfo";
import { InternetOffline } from './component';


const AppWrapper = () => {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  )
}

const App = () => {
  const [isOffline, setOfflineStatus] = useState(false);
  
  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
  
    return () => removeNetInfoSubscription();
  }, []);

  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={PersistStore}>
        {isOffline ? <InternetOffline/> : <AppWrapper />}
      </PersistGate>
    </Provider>

  )
}

export default App

const styles = StyleSheet.create({})