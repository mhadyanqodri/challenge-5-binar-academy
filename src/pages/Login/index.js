import { StyleSheet, Text, View, Alert, Image, TouchableOpacity, TextInput, ActivityIndicator } from 'react-native'
import React, { useState } from 'react'
import { loginImage } from "../../assets";
import Register from '../Register';
import { useDispatch, useSelector } from 'react-redux';
import { loginAction } from '../../redux/actions';



const Login = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const Loading = useSelector((state) => state.Login.loading);

  const dispatch = useDispatch()

  const onSubmit = () => {
    dispatch(loginAction(email, password, navigation))
  }

  return (
    <>
      {Loading ? <ActivityIndicator /> : (
        <View style={styles.container} testID="loginView">
          <View style={styles.loginImageContainer}>
            <Image source={loginImage} style={styles.loginImage}></Image>
          </View>
          <View style={styles.textInputContainer}>
            <TextInput testID='inputEmail' style={styles.input} placeholder="Email" value={email} onChangeText={(text) => setEmail(text)} placeholderTextColor={'black'} />
            <TextInput testID='inputPassword' style={styles.input} secureTextEntry={true} placeholder="Password" value={password} onChangeText={(text) => setPassword(text)} placeholderTextColor={'black'} />
          </View>
          <View style={styles.loginButton}>
            <TouchableOpacity testID='LoginButton' onPress={onSubmit}>
              <View style={styles.buttonContainer}>
                <Text style={styles.loginText}>Login</Text>
              </View>
            </TouchableOpacity>
          </View>
          <View style={styles.registerText}>
            <Text style={styles.label}>Don't have an account ?</Text>
          </View>
          <View style={styles.register}>
            <TouchableOpacity onPress={() => navigation.navigate(Register)}>
              <Text style={styles.label}>Register</Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </>
  )
}

export default Login

const styles = StyleSheet.create({
  text: {
    color: 'black',
    fontSize: 22
  },
  container: {
    flex: 1,
    backgroundColor: '#F7F5F2'
  },
  loginImageContainer: {
    alignItems: 'center',
  },
  loginImage: {
    width: 360,
    height: 230,
    marginTop: 80,
    borderRadius: 10,
  },
  textInputContainer: {
    marginTop: 20,
    alignItems: 'center'
  },
  input: {
    color: 'black',
    borderWidth: 1,
    width: 330,
    height: 40,
    borderRadius: 10,
    marginBottom: 10
  },
  buttonContainer: {
    backgroundColor: 'black',
    width: 280,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8
  },
  loginButton: {
    alignItems: 'center',
    marginTop: 10
  },
  registerText: {
    alignItems: 'center',
    marginTop: 5
  },
  label: {
    color: 'black',
    fontSize: 11
  },
  register: {
    alignItems: 'center',
    marginTop: 5
  },
  loginText: {
    color: 'white'
  }
})