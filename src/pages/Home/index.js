import { StyleSheet, Text, View, Dimensions, TouchableOpacity, ActivityIndicator, RefreshControl, ScrollView, Button } from 'react-native'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getActionPopular, getActionRecomeded, refresh } from '../../redux/actions'
import { CardRecomended, CardPopular } from '../../component'
import { LOGOUT_USER } from '../../redux/Types'
import Icon from 'react-native-vector-icons/FontAwesome'
import Media from '../Media'



const Home = ({ navigation }) => {
  const dispatch = useDispatch();
  const BookRecomended = useSelector((state) => state.home.bookRecomended);
  const BookPopular = useSelector((state) => state.home.bookPopular);
  const token = useSelector((state) => state.Login.token);
  const Loading = useSelector((state) => state.home.loading);
  const Refresh = useSelector((state) => state.home.isRefresh);

  const RefreshOn = () => {
    dispatch(refresh(true))
    dispatch(getActionRecomeded(token, 6));
    dispatch(getActionPopular(token));
  }


  const Logout = () => {
    dispatch({ type: LOGOUT_USER })
  }

  useEffect(() => {
    dispatch(getActionRecomeded(token, 6));
    dispatch(getActionPopular(token));
    // console.log(token)
  }, []);

  return (
    <>
      {Loading ? <ActivityIndicator /> : (
        <View testID='homeView' style={styles.container}>
          <ScrollView testID='scrollUpDown' showsVerticalScrollIndicator={false} refreshControl={
            <RefreshControl
              refreshing={Refresh}
              onRefresh={() => RefreshOn()}
            />
          }>
            <View style={styles.nameContainer}>
              <View style={styles.nameWrapper}>
                <Text style={styles.textLabel}>Good Morning Qodri</Text>
                <View style={styles.mediaButton}>
                  <TouchableOpacity onPress={() => navigation.navigate("Media")}>
                    <View style={styles.mediaTextWrapper}>
                      <Icon name='arrow-right' size={18} color="black"></Icon>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.logoutButtonWrapper}>
                  <TouchableOpacity title="Logout" onPress={Logout} style={styles.logoutButton} testID='LogoutButton' >
                    <View style={styles.logoutButton}>
                      <Icon name='power-off' size={18} color="black"></Icon>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={styles.recomended}>
              <View style={styles.labelWrapper}>
                <Text style={styles.recomendedText}>Recomended</Text>
              </View>
              <View>
                <ScrollView testID='scrollLeftRight' horizontal showsHorizontalScrollIndicator={false}>
                  {BookRecomended.map(item => (
                    <View key={item.id} style={styles.recomendedWrapper}>
                      <CardRecomended testID='recomendedCard' source={{ uri: item.cover_image }} onPress={() => navigation.navigate('Detail', { id: item.id })} />
                    </View>
                  ))}
                </ScrollView>
              </View>
            </View>
            <View style={styles.popular}>
              <View style={styles.labelWrapper}>
                <Text style={styles.popularText}>Popular Book</Text>
              </View>
              <View style={styles.popularContainer}>
                {BookPopular.map(item => (
                  <CardPopular testID='popularCard' key={item.id} source={{ uri: item.cover_image }} title={item.title} author={item.author} publisher={item.publisher} average_rating={item.average_rating} price={`Rp ${parseFloat(item.price).toLocaleString('id-ID')}`} onPress={() => navigation.navigate('Detail', { id: item.id })} />
                ))}
              </View>
            </View>
          </ScrollView>
        </View>
      )}
    </>
  )
}

export default Home

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    flex: 1
  },
  labelWrapper: {
    marginTop: 15,
    marginLeft: 15,
    marginBottom: 5
  },
  nameWrapper: {
    marginLeft: 15,
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center'
  },
  textLabel: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold'
  },
  recomendedCard: {
    width: windowWidth * 0.35,
    height: windowHeight * 0.28,
    borderRadius: 10,
  },
  recomendedContainer: {
    marginTop: 15,
  },
  popularContainer: {
    marginLeft: 10,
    marginTop: 10,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  nameContainer: {
    backgroundColor: 'black',
    height: windowHeight * 0.09,
  },
  recomended: {
    height: windowHeight * 0.41
  },
  recomendedWrapper: {
    flexDirection: 'row',
    marginHorizontal: windowWidth * 0.027,
  },
  logoutButton: {
    backgroundColor: 'white',
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20
  },
  logoutButtonWrapper: {
    paddingHorizontal: windowWidth * 0.05,
  },
  popular: {
    backgroundColor: 'black',
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    marginTop: -15
  },
  recomendedText: {
    fontSize: 20,
    color: 'black',
    fontWeight: '700'
  },
  popularText: {
    fontSize: 20,
    color: 'white',
    fontWeight: '700'
  },
  mediaText: {
    color: 'black'
  },
  mediaTextWrapper: {
    backgroundColor: 'white',
    width: 30,
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20
  },
  mediaButton: {
    marginLeft: 100
  }
})