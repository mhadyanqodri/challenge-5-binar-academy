import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import React, {useState} from 'react';
import { registerImage } from '../../assets';
import { TextInput } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Feather';
import Login from '../Login';
import Success from '../Success';
import { useDispatch, useSelector } from 'react-redux';
import { registerAction } from '../../redux/actions';

const Register = ({navigation}) => {
  const [fullname, setFullname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const Loading = useSelector((state) => state.register.loading);

  const dispatch = useDispatch()

  const onSubmit =() => {
    dispatch(registerAction(fullname, email, password, navigation))
  }


  return (
    <>
    { Loading ? <ActivityIndicator /> : (
        <View testID='registerView' style={styles.container}>
        <View>
          <TouchableOpacity onPress={() => navigation.navigate(Login)}>
            <View style={styles.backButton}>
              <Icon name='arrow-left' size={25} color="white" />
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.registerImageContainer}>
          <Image source={registerImage} style={styles.registerImage}></Image>
        </View>
        <View style={styles.textInputContainer}>
          <TextInput testID='inputName' style={styles.input} value={fullname} onChangeText={(text) => setFullname(text)} placeholder='Full Name' placeholderTextColor={'black'} />
          <TextInput testID='inputEmail' style={styles.input} value={email} onChangeText={(text) => setEmail(text)} placeholder='Email' placeholderTextColor={'black'} />
          <TextInput testID='inputPassword' style={styles.input} secureTextEntry={true} value={password} onChangeText={(text) => setPassword(text)} placeholder='Password' placeholderTextColor={'black'} />
        </View>
        <View style={styles.registerButton}>
          <TouchableOpacity testID='RegisterButton' onPress={onSubmit}>
            <View style={styles.registerButtonContainer}>
              <Text style={styles.registerText}>Register</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.loginText}>
          <Text style={styles.label}>Already have an account?</Text>
        </View>
        <View style={styles.login}>
          <TouchableOpacity onPress={() => navigation.navigate(Login)}>
            <Text style={styles.label}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    )}
    </>
  )
}

export default Register

const styles = StyleSheet.create({
  backButton: {
    marginLeft: 10,
    marginTop: 10,
    width: 30,
    height: 30,
    backgroundColor: 'black',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  registerImage: {
    width: 80,
    height: 80
  },
  registerImageContainer: {
    alignItems: 'center',
    marginTop: 120
  },
  input: {
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 10,
    width: 340,
    height: 40,
    color: 'black'
  },
  textInputContainer: {
    alignItems: 'center',
    marginTop: 20
  },
  registerButton: {
    alignItems: 'center',
    marginTop: 10
  },
  registerButtonContainer: {
    width: 300,
    height: 40,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10
  },
  loginText: {
    alignItems: 'center',
    marginTop: 10
  },
  label: {
    color: 'black',
    fontSize: 11
  },
  login: {
    alignItems: 'center',
    marginTop: 5
  },
  registerText: {
    color: 'white'
  }
})