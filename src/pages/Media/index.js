import { StyleSheet, Text, View, Button } from 'react-native'
import React from 'react'
import { video1, dokumen1 } from '../../assets'
import Video from 'react-native-video'
import Pdf from 'react-native-pdf'


const Media = () => {
    return (
        <View style={styles.mediaContainer}>
            <View style={styles.videoWrapper}>
                <Video
                    source={video1}
                    style={styles.homeVideo}
                    fullscreen={true}
                    controls={true}
                    poster='https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/English_Cocker_Spaniel_4.jpg/800px-English_Cocker_Spaniel_4.jpg'
                />
            </View>
            <View style={styles.documentWrapper}>
                <Button title='Open PDF Document' />
                <Pdf
                    source={dokumen1}
                    style={styles.homeDocument}
                />
            </View>
        </View>
    )
}

export default Media

const styles = StyleSheet.create({
    homeVideo: {
        width: 200,
        height: 200,
    },
    videoWrapper: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    documentWrapper: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20
    },
    mediaContainer: {
        backgroundColor: '#3A3845',
        height: 500
    },
    homeDocument: {
        width: 300,
        height: 300,
        marginTop: 10
    }
})