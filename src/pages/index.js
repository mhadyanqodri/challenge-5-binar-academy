import Splash from "./Splash";
import Login from "./Login";
import Register from "./Register";
import Success from "./Success";
import Home from "./Home";
import Detail from "./Detail";
import Media from "./Media";

export { Splash, Login, Register, Success, Home, Detail, Media };
