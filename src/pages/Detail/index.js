import { StyleSheet, Text, View, TouchableOpacity, Dimensions, Share, ActivityIndicator, ScrollView, RefreshControl } from 'react-native'
import React, { useState, useEffect } from 'react'
import Icon from 'react-native-vector-icons/Feather';
import Home from '../Home';
import { getDetailAction, refresh } from '../../redux/actions';
import { useDispatch, useSelector } from 'react-redux'
import { CardDescription, LikeButton, OthersDescription } from "../../component";
import { configure, buatChannel, kirimNotifikasi } from '../../utils';

const Detail = ({ navigation, route }) => {

  const Detail = useSelector(data => data.getDetail.detail)
  const dispatch = useDispatch();
  const token = useSelector((state) => state.Login.token);
  const { id } = route.params;
  const Loading = useSelector((data) => data.getDetail.loading);
  const Refresh = useSelector((data) => data.getDetail.isRefresh);

  const klikTombol = () => {
  configure();
  buatChannel("1");
  kirimNotifikasi("1", `${Detail.title}`, "Anda Menyukai buku ini");
  }

  const RefreshOn = () => {
    dispatch(refresh(true))
    dispatch(getDetailAction(token, id));
  }

  const onShare = async () => {
    try {
      const result = await Share.share({
        message:
          `go to home for detail movies ${Detail.title}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  useEffect(() => {
    dispatch(getDetailAction(token, id));
    // console.log("Detail", Detail)
    // console.log(token)
  }, []);

  return (
    <>
      {Loading ? <ActivityIndicator /> : (
        <View style={styles.container}>
          <ScrollView refreshControl={
            <RefreshControl
              refreshing={Refresh}
              onRefresh={() => RefreshOn()}
            />}>
            <View style={styles.navbar}>
              <View style={styles.backButtonWrapper}>
                <TouchableOpacity onPress={() => navigation.navigate(Home)}>
                  <View style={styles.backButton}>
                    <Icon name='arrow-left' size={25} color="#DDDDDD" />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.likeButtonWrapper}>
                <TouchableOpacity onPress={klikTombol}>
                  {/* <View style={styles.likeButton}>
                    <Icon name='heart' size={20} color="#DDDDDD" />
                  </View> */}
                    <LikeButton title="love" />
                </TouchableOpacity>
              </View>
              <View style={styles.shareButtonWrapper}>
                <TouchableOpacity onPress={onShare}>
                  <View style={styles.shareButton}>
                    <Icon name='share' size={19} color="#DDDDDD" />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.description}>
              <CardDescription source={{ uri: Detail.cover_image }} title={Detail.title} author={Detail.author} publisher={Detail.publisher} />
            </View>
            <View style={styles.others}>
              <OthersDescription average_rating={Detail.average_rating} total_sale={Detail.total_sale} price={`Rp ${parseFloat(Detail.price).toLocaleString('id-ID')}`} />
            </View>
            <View style={styles.overview}>
              <View style={styles.labelWrapper}>
                <Text style={styles.label}>Overview</Text>
              </View>
              <View style={styles.synopsisWrapper}>
                <Text style={styles.overviewText}>{Detail.synopsis}</Text>
              </View>
            </View>
          </ScrollView>
        </View>
      )}
    </>
  )
}

export default Detail

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#DDDDDD',
    flex: 1,
  },
  backButton: {
    marginLeft: 10,
    marginTop: 10,
    width: 30,
    height: 30,
    backgroundColor: '#3A3845',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  likeButton: {
    marginLeft: 10,
    marginTop: 10,
    width: 30,
    height: 30,
    backgroundColor: '#3A3845',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  shareButton: {
    marginLeft: 10,
    marginTop: 10,
    width: 30,
    height: 30,
    backgroundColor: '#3A3845',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center'
  },
  navbar: {
    flexDirection: 'row'
  },
  likeButtonWrapper: {
    marginLeft: windowWidth * 0.58
  },
  shareButtonWrapper: {
    marginLeft: windowWidth * 0.04
  },
  backButtonWrapper: {
    marginHorizontal: windowWidth * 0.013
  },
  others: {
    backgroundColor: 'white',
    height: windowHeight * 0.1,
    justifyContent: 'center'
  },
  labelWrapper: {
    paddingLeft: windowWidth * 0.1,
    paddingTop: windowHeight * 0.03
  },
  label: {
    color: 'black',
    fontSize: 20,
    fontWeight: 'bold'
  },
  overviewText: {
    paddingHorizontal: windowWidth * 0.1,
    marginTop: 10,
    color: 'black',
    fontSize: 15,
  },
  overview: {
    marginBottom: 25,
  },
  synopsisWrapper: {
    alignContent: 'center'
  }
})