import {
    LOGIN_SUCCESS,
    LOGIN_FAILED,
    LOGIN_LOADING,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    REGISTER_LOADING,
    GET_RECOMENDED_SUCCESS,
    GET_POPULAR_SUCCESS,
    GET_FAILED,
    GET_LOADING,
    GET_DETAIL_SUCCES,
    GET_DETAIL_FAILED,
    GET_DETAIL_LOADING,
    LOGOUT_USER,
    REFRESH
} from "../Types";

const initialLoginState = {
    token: null,
    loading: false,
    errorMessage: '',
};

const initialRegisterState = {
    token: null,
    loading: false,
    errorMessage: '',
};

const initialGetData = {
    bookRecomended: [],
    bookPopular: [],
    loading: false,
    errorMessage: '',
    isRefresh: false
};

const initialgetDetail = {
    detail: [],
    loading: false,
    errorMessage: '',
    isRefresh: false
};


export const loginReducer = (state = initialLoginState, action) => {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return {
                ...state,
                token: action.payload,
                loading: false
            };

        case LOGIN_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };

        case LOGIN_LOADING:
            return {
                ...state,
                loading: action.payload
            };

        case LOGOUT_USER:
            return {
                ...state,
                token: null
            };

        default:
            return state;
    }
}

export const registerReducer = (state = initialRegisterState, action) => {
    switch (action.type) {
        case REGISTER_SUCCESS:
            return {
                ...state,
                token: action.payload,
                loading: false
            };

        case REGISTER_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };

        case REGISTER_LOADING:
            return {
                ...state,
                loading: action.payload
            };

        default:
            return state;
    }
}

export const getHomeReducer = (state = initialGetData, action) => {
    switch (action.type) {
        case GET_RECOMENDED_SUCCESS:
            return {
                ...state,
                bookRecomended: action.payload,
                loading: false
            }

        case GET_POPULAR_SUCCESS:
            return {
                ...state,
                bookPopular: action.payload,
                loading: false
            };

        case GET_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };

        case GET_LOADING:
            return {
                ...state,
                loading: action.payload
            };

        case REFRESH:
            return {
                ...state,
                isRefresh: action.payload
            }

        default:
            return state;
    }
}

export const getDetailReducer = (state = initialgetDetail, action) => {
    switch (action.type) {
        case GET_DETAIL_SUCCES:
            return {
                ...state,
                detail: action.payload,
                loading: false
            };

        case GET_DETAIL_FAILED:
            return {
                ...state,
                errorMessage: action.payload,
                loading: false
            };

        case GET_DETAIL_LOADING:
            return {
                ...state,
                loading: action.payload
            };

        case REFRESH:
            return {
                ...state,
                isRefresh: action.payload
            }

        default:
            return state;
    }
}