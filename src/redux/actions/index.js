import { 
    LOGIN_SUCCESS, 
    LOGIN_FAILED, 
    LOGIN_LOADING,
    REGISTER_SUCCESS,
    REGISTER_FAILED,
    REGISTER_LOADING,
    GET_RECOMENDED_SUCCESS,
    GET_POPULAR_SUCCESS,
    GET_FAILED,
    GET_LOADING,
    GET_DETAIL_SUCCES,
    GET_DETAIL_FAILED,
    GET_DETAIL_LOADING,
    REFRESH,
} from "../Types";

import {Alert} from "react-native"

import axios from "axios";

export const loginSuccess = (data) => ({
    type: LOGIN_SUCCESS,
    payload: data,
});

export const loginFailed = (data) => ({
    type: LOGIN_FAILED,
    payload: data,
});

export const loginLoading = (data) => ({
    type: LOGIN_LOADING,
    payload: data,
});

export const registerSuccess = (data) => ({
    type: REGISTER_SUCCESS,
    payload: data,
});

export const registerFailed = (data) => ({
    type: REGISTER_FAILED,
    payload: data,
});

export const registerLoading = (data) => ({
    type: REGISTER_LOADING,
    payload: data,
})

export const getDataRecomended = (data) => ({
    type: GET_RECOMENDED_SUCCESS,
    payload: data,
})

export const getDataPopular = (data) => ({
    type: GET_POPULAR_SUCCESS,
    payload: data,
})

export const getFailed = (data) => ({
    type: GET_FAILED,
    payload: data,
})

export const getLoading = (data) => ({
    type: GET_LOADING,
    payload: data,
})

export const getDetail = (data) => ({
    type: GET_DETAIL_SUCCES,
    payload: data,
});

export const getDetailFailed = (data) => ({
    type: GET_DETAIL_FAILED,
    payload: data,
});

export const getDetailLoading = (data) => ({
    type: GET_DETAIL_LOADING,
    payload: data,
});

export const refresh = (data) => ({
    type: REFRESH,
    payload: data,
})

export const loginAction = (email, password, navigation) => async (dispatch) => {
    dispatch(getLoading(true))
    try {
        await axios.post('http://code.aldipee.com/api/v1/auth/login', {
            email: email,
            password: password
        }).then((res) => {
            if (res.data.tokens.access.token){
                dispatch(loginSuccess(res.data.tokens.access.token))
            }
        })
    } catch (error) {
        dispatch(loginFailed(error.message))
        console.log(error.message)
    }
}

export const registerAction = (fullname, email, password, navigation) => async (dispatch) => {
    dispatch(getLoading(true))
    try {
        await axios.post('http://code.aldipee.com/api/v1/auth/register', {
            name: fullname,
            email: email,
            password: password
        }).then((res) => {
            navigation.navigate("Success")
            if (res.data.message){
                dispatch(res.data.message)
            }
        })
    } catch (error) {
        dispatch(registerFailed(error.message))
        console.log(error.message)
    }
}

export const getActionRecomeded = (token, limit) => async (dispatch) => {
    dispatch(getLoading(true))
    try {
        await axios.get('http://code.aldipee.com/api/v1/books', {
            headers: {
                Authorization: `Bearer ${token}`}, 
                params: {limit}
        }).then((res) => {
            dispatch(refresh(false))
            dispatch(getDataRecomended(res.data.results))
        })
    } catch (error) {
        dispatch(refresh(false))
        // Alert.alert(error.message)
        dispatch(getFailed(error.message))
        console.log(error.message)
    }
}

export const getActionPopular = (token) => async (dispatch) => {
    dispatch(getLoading(true))
    try {
        await axios.get('http://code.aldipee.com/api/v1/books', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then((res) => {
            dispatch(refresh(false))
            dispatch(getDataPopular(res.data.results))
        })
    } catch (error) {
        dispatch(refresh(false))
        // Alert.alert(error.message)
        dispatch(getFailed(error.message))
        console.log(error.message)
    }
}

export const getDetailAction = (token, id) => async (dispatch) => {
    // dispatch({type: LOGIN_LOADING})
    dispatch(getDetailLoading(true))
    try {
        await axios.get(`http://code.aldipee.com/api/v1/books/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).then((res) => {
            dispatch(refresh(false))
            console.log('respone', res.data)
            dispatch(getDetail(res.data))
        })
    } catch (error) {
        dispatch(refresh(false))
        dispatch(getDetailFailed(error.message))
        console.log(error.message)
    }
}

