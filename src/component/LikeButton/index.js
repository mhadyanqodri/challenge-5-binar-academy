import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/Feather';

const LikeButton = ({onPress, Title}) => {

    function LikeIcon() {
        if(Title == 'love') return onPress ? <Icon name='heart' size={20} color="red"/> : <Icon name='heart' size={20} color="#DDDDDD"/>

        return <Icon name='heart' size={20} color="#DDDDDD" />;
    }

    return (
        <View style={styles.likeButton}>
            <LikeIcon />
        </View>
    )
}

export default LikeButton

const styles = StyleSheet.create({
    likeButton: {
        marginLeft: 10,
        marginTop: 10,
        width: 30,
        height: 30,
        backgroundColor: '#3A3845',
        borderRadius: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
})