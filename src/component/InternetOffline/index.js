import { StyleSheet, Text, View, Image } from 'react-native'
import React from 'react'
import { offlineImg } from "../../assets";


const InternetOffline = () => {
    return (
        <View style={styles.page}>
            <Image source={offlineImg} style={styles.image}></Image>
            <Text style={styles.offlineText}>You are currently offline</Text>
        </View>
    )
}

export default InternetOffline

const styles = StyleSheet.create({
    offlineText: {
        color: 'black',
        marginLeft: 125
    },
    image: {
        marginTop: 100,
        marginLeft: 150
    },
    page: {
        backgroundColor: '#EFEFEF',
        flex: 1
    }
})