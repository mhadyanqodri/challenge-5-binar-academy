import { StyleSheet, View, TouchableOpacity, Image, Dimensions, Text } from 'react-native'
import React from 'react'

const CardPopular = ({ source, onPress, title, author, publisher, average_rating, price }) => {
    return (
        <View style={styles.popularWrapper}>
            <TouchableOpacity onPress={onPress}>
                <Image source={source} style={styles.popularCard}></Image>
            </TouchableOpacity>
            <View style={styles.description}>
                <Text style={styles.judulFilm} numberOfLines={1} ellipsizeMode='tail'>{title}</Text>
                <Text style={styles.descriptionText}>{author}</Text>
                <Text style={styles.descriptionText}>{publisher}</Text>
                <Text style={styles.rating}>{average_rating}</Text>
                <Text style={styles.harga}>{price}</Text>
            </View>
        </View>
    )
}

export default CardPopular

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
    popularWrapper: {
        marginBottom: 20,
        width: windowWidth*0.31
    },
    popularCard: {
        width: windowWidth * 0.28,
        height: windowHeight * 0.23,
        borderRadius: 10,
        marginLeft: 8,
    },
    description: {
        alignItems: 'center',
    },
    judulFilm: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 12
    },
    descriptionText: {
        color: 'white',
        fontSize: 9,
    },
    rating: {
        color: 'white',
        fontSize: 11,
        fontWeight: 'bold'
    },
    harga: {
        color: 'white',
        fontSize: 13,
        fontWeight: 'bold'
    },
})