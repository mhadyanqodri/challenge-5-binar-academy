describe('Example', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    // await device.reloadReactNative();
  });

  it('should have Register screen', async () => {
    await expect(element(by.id('registerView'))).toBeVisible();
  });

  it('should fill the Register form', async () => {
    await element(by.id('inputName')).typeText('fullnameee123'); //diubah setiap 1x test
    await element(by.id('inputEmail')).typeText('example12345@gmail.com'); //diubah setiap 1x test
    await element(by.id('inputPassword')).typeText('example123\n');
    await element(by.id('RegisterButton')).tap();
  });

  it('should have Success screen', async () => {
    await expect(element(by.id('successView'))).toBeVisible();
  });

  it('should try the Success button', async () => {
    await element(by.id('SuccessButton')).tap();
  });

  it('should have Login screen', async () => {
    await expect(element(by.id('loginView'))).toBeVisible();
  });

  it('should fill the Login form', async () => {
    await element(by.id('inputEmail')).typeText('qodri@gmail.com');
    await element(by.id('inputPassword')).typeText('qodri123\n');
    await element(by.id('LoginButton')).tap();
  });

  it('should have Home screen', async () => {
    await expect(element(by.id('loginView'))).toBeVisible();
  });

  it('should try all the function in Home screen', async () => {
    await element(by.id('scrollLeftRight')).swipe("left");
    await element(by.id('scrollUpDown')).scrollTo("bottom");
    await element(by.id('LogoutButton')).tap();
  });

});
